﻿-- ------------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           10.1.9-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para neppo
CREATE DATABASE IF NOT EXISTS `neppo` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `neppo`;

-- Copiando estrutura para tabela neppo.pessoa
CREATE TABLE IF NOT EXISTS `pessoa` (
  `pessoa_id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(80) CHARACTER SET latin1 NOT NULL,
  `nascimento` date NOT NULL,
  `rg` varchar(15) CHARACTER SET latin1 NOT NULL,
  `sexo` char(1) CHARACTER SET latin1 NOT NULL,
  `endereco` varchar(100) CHARACTER SET latin1 NOT NULL,
  `idade` varchar(3) NOT NULL,
  PRIMARY KEY (`pessoa_id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela neppo.pessoa: 14 rows
/*!40000 ALTER TABLE `pessoa` DISABLE KEYS */;
INSERT INTO `pessoa` (`pessoa_id`, `nome`, `nascimento`, `rg`, `sexo`, `endereco`, `idade`) VALUES
	(6, 'André Assunçãobbbbbbb', '1987-11-09', '1381876-9bbbb', 'M', 'ãõéúçêâbbbbbbb', '41'),
	(23, 'Fred Mercury xxx', '1996-04-17', '1234567xxxx', 'F', 'rua nova xxxx', '60'),
	(9, 'Marcela Marques', '2005-09-17', '1381876-9', 'F', 'Rua da Constelação, 45 - Alvorada', '30'),
	(10, 'Maria Joaquina', '2002-09-17', '1381876-9', 'F', 'Rua da Constelação, 45 - Alvorada', '18'),
	(11, 'Maria Joaquina Cirilo', '1998-09-18', '1381876-10', 'F', 'Rua da Constelação, 45 - Alvorada III', '5'),
	(12, 'Fred Mercury9999999', '1999-04-17', '12345679999999', 'F', 'rua nova9999999', '38'),
	(13, 'Maria Batania', '1999-08-17', '1381876-9', 'F', 'Rua da Constelação, 45 - Alvorada', '45'),
	(15, 'Joao Bosco', '2001-08-26', '321654987-98', 'F', 'rua beco avenida', '55'),
	(17, 'Manoel Joaquim', '2008-09-17', '1381876-9', 'F', 'Rua da Constelação, 45 - Alvorada', '22'),
	(19, 'Marcelo  Dedeco', '1978-11-09', '8798655421-98', 'M', 'Rua Inacio Guimaraes. 54 - Santa Luzia', '33'),
	(21, 'Luana Correia', '1982-08-15', '56875421-45', 'F', 'Rua da Bomba, 300 - Parque 10', '20'),
	(22, 'Roseana Carvalho', '1996-01-17', '3265542154875-9', 'F', 'Rua Jonatas Pedrosa, 45 - Sao Lazaro', '37');
/*!40000 ALTER TABLE `pessoa` ENABLE KEYS */;

-- Copiando estrutura para procedure neppo.sp_graficoFaixaIdade
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_graficoFaixaIdade`()
BEGIN
DECLARE cont09, cont1019, cont2029, cont3039, cont40 INT;

		 
		Select COUNT(*) as total into cont09 
		FROM pessoa 
		where idade BETWEEN 0 AND 9;
		
		Select COUNT(*) as total into cont1019 
		FROM pessoa 
		where idade BETWEEN 10 AND 19;
		
		Select COUNT(*) as total into cont2029 
		FROM pessoa 
		where idade BETWEEN 20 AND 29;
		
		Select COUNT(*) as total into cont3039
		FROM pessoa 
		where idade BETWEEN 30 AND 39;
		
		Select COUNT(*) as total into cont40 
		FROM pessoa 
		where idade >= 40;
		
		
		select cont09, cont1019, cont2029, cont3039, cont40;
END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
