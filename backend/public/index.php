<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../src/config/db.php';

$app = new \Slim\App;

//$app->contentType('application/json; charset=utf-8');
//$app->contentType('text/html; charset=utf-8');
//$app->response()->header('Content-Type', 'application/json;charset=utf-8'); 

/*$app->get('/hello/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});*/

//customers routes
require '../src/routes/pessoa.php';

$app->run();