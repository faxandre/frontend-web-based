<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

/**
*  Busca todas as pessoas
*/
$app->get('/api/pessoa', function (Request $request, Response $response) {

	$sql = "select * from pessoa order by nome";

	try {
	 	$db = new db(); 
	 	$db = $db->connect();
	 	$stmt = $db->query($sql);
	 	$pessoas = $stmt->fetchAll(PDO::FETCH_OBJ);
		
		foreach( $pessoas as $key=>$pessoa)
			$pessoa->nascimento = date('d/m/Y', strtotime(str_replace('-', '/', $pessoa->nascimento)));

		$db = null;

	 	return json_encode($pessoas);

	} catch(PDOException $e) {
		echo json_encode(array(
			"erro" => str_replace("'", "", $e->getMessage())
		));	
	}

});

/**
*  Busca pessoa pelo ID
*/
$app->get('/api/pessoa/{id}', function (Request $request, Response $response) {
	
	$id = $request->getAttribute('id');
	$sql = "select * from pessoa where pessoa_id = $id";

	try {
	 	$db = new db(); 
	 	$db = $db->connect();
	 	$stmt = $db->query($sql);
	 	$pessoa = $stmt->fetchAll(PDO::FETCH_OBJ);

	 	$db = null;
		
		echo json_encode(array(
			"pessoa_id"  => $pessoa[0]->pessoa_id,
			"nome"       => $pessoa[0]->nome,
			"nascimento" => date('d/m/Y', strtotime(str_replace('-', '/', $pessoa[0]->nascimento ))),
			"rg"         => $pessoa[0]->rg,
			"sexo"       => $pessoa[0]->sexo,
			"endereco"   => $pessoa[0]->endereco,
			"idade"      => $pessoa[0]->idade
		));
		

	} catch(PDOException $e) {
		echo json_encode(array(
			"erro" => str_replace("'", "", $e->getMessage())
		));	
	}

});

/**
*  Adiciona pessoa
*/
$app->post('/api/pessoa', function (Request $request, Response $response) {
	
	$nome       = $request->getParam('nome');
	$nascimento = date('Y-m-d', strtotime(str_replace('/', '-', $request->getParam('nascimento') )));
	$rg         = $request->getParam('rg');
	$sexo       = $request->getParam('sexo');
	$endereco   = $request->getParam('endereco');
	$idade      = $request->getParam('idade');

	if($nome == '' or $nascimento == '' or $rg == '' or $sexo == '' or $endereco == '' or $idade == '' ){
		echo json_encode(array(
			"validation" => 'Todos os campos são obrigatórios!'
		));
		exit;
	}
	
	$sql = "insert into pessoa values (0, :nome, :nascimento, :rg, :sexo, :endereco, :idade)";

	try {
	 	$db = new db(); 
	 	$db = $db->connect();
	 	$stmt = $db->prepare($sql);

	 	$stmt->bindParam(':nome',       $nome);
	 	$stmt->bindParam(':nascimento', $nascimento);
	 	$stmt->bindParam(':rg',         $rg);
	 	$stmt->bindParam(':sexo',       $sexo);
	 	$stmt->bindParam(':endereco',   $endereco);
	 	$stmt->bindParam(':idade',      $idade);

		$stmt->execute();
		$pessoa_id = $db->lastInsertId();
		
	 	$db = null;

		
		echo json_encode(array(
				"pessoa_id"  => $pessoa_id,
				"nome"       => $nome,
				"nascimento" => date('d/m/Y', strtotime(str_replace('-', '/', $nascimento ))),
				"rg"         => $rg,
				"sexo"       => $sexo,
				"endereco"   => $endereco,
				"idade"      => $idade
			));

	} catch(PDOException $e) {
		echo json_encode(array(
			"erro" => str_replace("'", "", $e->getMessage())
		));
	}

});

/**
*  Atualiza pessoa
*/
$app->put('/api/pessoa/{id}', function (Request $request, Response $response) {
	
	$id = $request->getAttribute('id');

	$nome       = $request->getParam('nome');
	$nascimento = date('Y-m-d', strtotime(str_replace('/', '-', $request->getParam('nascimento') )));	
	$rg         = $request->getParam('rg');
	$sexo       = $request->getParam('sexo');
	$endereco   = $request->getParam('endereco');
	$idade      = $request->getParam('idade');

	$sql = "update pessoa set 
				nome       = :nome, 
				nascimento = :nascimento, 
				rg         = :rg, 
				sexo       = :sexo, 
				endereco   = :endereco,
				idade      = :idade
			 where pessoa_id = $id";

	try {
	 	$db = new db(); 
	 	$db = $db->connect();
	 	$stmt = $db->prepare($sql);

	 	$stmt->bindParam(':nome',       $nome);
	 	$stmt->bindParam(':nascimento', $nascimento);
	 	$stmt->bindParam(':rg',         $rg);
	 	$stmt->bindParam(':sexo',       $sexo);
	 	$stmt->bindParam(':endereco',   $endereco);
	 	$stmt->bindParam(':idade',      $idade);

		$stmt->execute();

	 	$db = null;

		echo json_encode(array(
			"pessoa_id"  => $id,
			"nome"       => $nome,
			"nascimento" => date('d/m/Y', strtotime(str_replace('-', '/', $nascimento ))),
			"rg"         => $rg,
			"sexo"       => $sexo,
			"endereco"   => $endereco,
			"idade"      => $idade
		));

	} catch(PDOException $e) {
		echo json_encode(array(
			"erro" => str_replace("'", "", $e->getMessage())
		));
	}

});

/**
*  Busca pessoa pelo ID
*/
$app->delete('/api/pessoa/{id}', function (Request $request, Response $response) {
	
	$id = $request->getAttribute('id');
	$sql = "delete from pessoa where pessoa_id = $id";
	
	try {
	 	$db = new db(); 
	 	$db = $db->connect();
	 	$stmt = $db->prepare($sql);
	 	$stmt->execute();
		//$stmt->rowCount(); //insert, update, delete
	 	$db = null;

		echo $stmt->rowCount();		

	} catch(PDOException $e) {
		echo json_encode(array(
			"erro" => str_replace("'", "", $e->getMessage())
		));
	}

});




/**
*  Grafico - Faixa de Idade
*/
$app->options('/api/pessoa/g1', function (Request $request, Response $response) {
	try {

	 	$db = new db(); 
	 	$db = $db->connect();
	 	$stmt = $db->query("CALL sp_graficoFaixaIdade();");
	 	$result = $stmt->fetchAll(PDO::FETCH_ASSOC); 

		echo   
		'{
			"cols": [{"id":"andre", "label":"dedeco", "pattern":"", "type":"string"}, {"id":"", "label":"",  "pattern":"",  "type":"number"}],
			"rows": [
				{"c":[ {"v":"0 a 9",        "f":null}, {"v":' .(int)$result[0]['cont09'].   ', "f":null} ]},
				{"c":[ {"v":"10 a 19",      "f":null}, {"v":' .(int)$result[0]['cont1019']. ', "f":null} ]},
				{"c":[ {"v":"20 a 29",      "f":null}, {"v":' .(int)$result[0]['cont2029']. ', "f":null} ]},
				{"c":[ {"v":"30 a 39",      "f":null}, {"v":' .(int)$result[0]['cont3039']. ', "f":null} ]},
				{"c":[ {"v":"Maior que 40", "f":null}, {"v":' .(int)$result[0]['cont40'].   ', "f":null} ]}
			]
		}'; 

	} catch(PDOException $e) {
		echo json_encode(array(
			"erro" => str_replace("'", "", $e->getMessage())
		));
	}

});


/**
*  Grafico - Quantidade de Pessoas por Sexo
*/
$app->options('/api/pessoa/g2', function (Request $request, Response $response) {

	$sql = "SELECT sexo AS sexo , COUNT(sexo) AS qtd 
			FROM pessoa 
			GROUP BY sexo";
	
	try {

	 	$db = new db(); 
	 	$db = $db->connect();
	 	$stmt = $db->query($sql);
	 	$result = $stmt->fetchAll(PDO::FETCH_ASSOC); 

		$head[0] = Array('id' => '', 'label' => '', 'pattern' => '', 'type' => 'string');
		$head[1] = Array('id' => '', 'label' => '', 'pattern' => '', 'type' => 'number');

	 	for ($i = 0; $i < count($result); $i++) {
			$newResult[$i]['c'] = 	Array(
										Array('v' => $result[$i]['sexo'] == 'M' ? 'Masculino' : 'Feminino', 'f' => null),
										Array('v' => (int)$result[$i]['qtd'], 'f' => null),
	 							  	);
	 	}

	 	$arrayGrafico['cols'] = $head;
		$arrayGrafico['rows'] = $newResult;

	 	echo json_encode($arrayGrafico);

	} catch(PDOException $e) {
		echo json_encode(array(
			"erro" => str_replace("'", "", $e->getMessage())
		));
	}	

});