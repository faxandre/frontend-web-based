<?php
 
 class db {

 	private $host = "localhost";
 	private $db   = "neppo";
 	private $user = "root";
 	private $pass = "";
 	
 	public function connect(){
 		//$string = "mysql:host=localhost:3306;dbname=frontendwebbased";
 		$string = "mysql:host=$this->host;dbname=$this->db;charset=utf8";
		$conn = new PDO($string, $this->user, $this->pass);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//$conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		return $conn;
 	}

}

